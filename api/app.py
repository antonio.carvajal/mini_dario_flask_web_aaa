import random

from flask import Flask, request, jsonify

app = Flask(__name__)


USERS = {
    "1": {
        "user": "admin",
        "password": "admin",
        "user_type": "admin"
    },
    "2": {
        "user": "guest",
        "password": "guest",
        "user_type": "guest"
    }
}


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/')
@app.route('/index/')
def index():
    return ("Este es el index para ver que todo funciona bien")


@app.route('/login/', methods=['POST'])
def user_login():
    req_data = request.get_json()
    try:
        user = req_data['user']
        password = req_data['password']
    except Exception:
        raise InvalidUsage('Did not introduce user or password', status_code=400)
    user_type = exist_user_and_get_type(user, password)
    if not user_type:
        raise InvalidUsage('Username or password incorrect', status_code=400)
    response = {
        "token": random.randint(100000, 999999),
        "user_type": user_type
    }
    return jsonify(response)


def exist_user_and_get_type(user, password):
    for _, value in USERS.items():
        if user == value['user'] and password == value['password']:
            return value['user_type']
    return None


if __name__ == '__main__':
    app.run(debug=True)
